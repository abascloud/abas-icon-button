
<!---

This README is automatically generated from the comments in these files:
abas-icon-button-light.html  abas-icon-button.html

Edit those files, and our readme bot will duplicate them over here!
Edit this file, and the bot will squash your changes :)

The bot does some handling of markdown. Please file a bug if it does the wrong
thing! https://github.com/PolymerLabs/tedium/issues

-->

## &lt;abas-icon-button&gt;

Material design: [Icon toggles](https://www.google.com/design/spec/components/buttons.html#buttons-toggle-buttons)

`abas-icon-button` is a button with an image placed at the center. When the user touches
the button, a ripple effect emanates from the center of the button.

`abas-icon-button` does not include a default icon set. To use icons from the default
set, include `abas-icons.html`, and use the `icon` attribute to specify which icon
from the icon set to use.

Example:

```html
<link href="path/to/abas-icons/abas-icons.html" rel="import">

<abas-icon-button icon="abas:ball_green"></abas-icon-button>
<abas-icon-button icon="abas:ball_red" nodefaultcolor></abas-icon-button>
```

To use `abas-icon-button` as a link, wrap it in an anchor tag. Since `abas-icon-button`
will already receive focus, you may want to prevent the anchor tag from receiving focus
as well by setting its tabindex to -1.

```html
<a href="https://abas-erp.com" tabindex="-1">
  <abas-icon-button icon="abas:ball_green"></abas-icon-button>
</a>
```

### Styling

Style the button with CSS as you would a normal DOM element. If you are using the icons
provided by `iron-icons`, they will inherit the foreground color of the button.

```html
/* make a "ball_green" icon button red */
<abas-icon-button icon="abas:ball_green" nodefaultcolor></abas-icon-button>
<style is="custom-style">
  abas-icon {
    --abas-icon-fill-color: #FF0000;
  }
</style>
```

By default, the ripple is the same color as the foreground at 25% opacity. You may
customize the color using the `--abas-icon-button-ink-color` custom property.

The following custom properties and mixins are available for styling:

| Custom property | Description | Default |
| --- | --- | --- |
| `--abas-icon-fill-color` | Mixin applied to fill abas icon color | `currentcolor`
| `--abas-icon-button-disabled-text` | The color of the disabled button | `--disabled-text-color` |
| `--abas-icon-button-ink-color` | Selected/focus ripple color | `--primary-text-color` |
| `--abas-icon-button` | Mixin for a button | `{}` |
| `--abas-icon-button-disabled` | Mixin for a disabled button | `{}` |
| `--abas-icon-button-hover` | Mixin for button on hover | `{}` |

